#Lots borrowed from JZJCoreSoftware
#NOTE: make flash will create an unoptimized build by default. Use another make target first

BUILDDIR = ./build/
SRCDIR = ./src/
INCDIR =./include/

CC = avr-gcc
CFLAGS = -Wall -DF_CPU=16000000 -mmcu=atmega328p -std=gnu17
COMMONOPTIMIZINGCFLAGS = -flto -fuse-linker-plugin -fmerge-all-constants
INC = -I$(INCDIR)

DEPS = $(INCDIR)/avreeprom.h $(INCDIR)/buzzer.h $(INCDIR)/i2c.h $(INCDIR)/lcd.h $(INCDIR)/rtc.h $(INCDIR)/ui/alarm.h $(INCDIR)/ui/clock.h $(INCDIR)/ui/menu.h $(INCDIR)/ui/ui.h

#Target-specific variable values: https://stackoverflow.com/questions/1079832/how-can-i-configure-my-makefile-for-debug-and-release-builds
.PHONY: release debug size fast
release: CFLAGS += -O3  $(COMMONOPTIMIZINGCFLAGS) -DNDEBUG
debug: CFLAGS += -DDEBUG -g -Og
size: CFLAGS += -Os $(COMMONOPTIMIZINGCFLAGS)  -DNDEBUG
fast: CFLAGS += -Ofast $(COMMONOPTIMIZINGCFLAGS) -DNDEBUG
release: $(BUILDDIR)/a.out.hex showSize
debug: $(BUILDDIR)/a.out.hex showSize
size: $(BUILDDIR)/a.out.hex showSize
fast: $(BUILDDIR)/a.out.hex showSize

#Linking

$(BUILDDIR)/a.out.hex: $(BUILDDIR)/a.out | $(BUILDDIR)
	avr-objcopy -O ihex $(BUILDDIR)/a.out $(BUILDDIR)/a.out.hex

$(BUILDDIR)/a.out: $(BUILDDIR)/avreeprom.o $(BUILDDIR)/buzzer.o $(BUILDDIR)/i2c.o $(BUILDDIR)/lcd.o $(BUILDDIR)/main.o $(BUILDDIR)/rtc.o $(BUILDDIR)/ui/alarm.o $(BUILDDIR)/ui/clock.o $(BUILDDIR)/ui/menu.o $(BUILDDIR)/ui/ui.o | $(BUILDDIR)
	avr-gcc $(CFLAGS) $(BUILDDIR)/avreeprom.o $(BUILDDIR)/buzzer.o $(BUILDDIR)/i2c.o $(BUILDDIR)/lcd.o $(BUILDDIR)/main.o $(BUILDDIR)/rtc.o $(BUILDDIR)/ui/alarm.o $(BUILDDIR)/ui/clock.o $(BUILDDIR)/ui/menu.o $(BUILDDIR)/ui/ui.o -o $(BUILDDIR)/a.out

#Object files

$(BUILDDIR)/avreeprom.o: $(SRCDIR)/avreeprom.c $(DEPS) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/avreeprom.c -o $(BUILDDIR)/avreeprom.o

$(BUILDDIR)/buzzer.o: $(SRCDIR)/buzzer.c $(DEPS) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/buzzer.c -o $(BUILDDIR)/buzzer.o

$(BUILDDIR)/i2c.o: $(SRCDIR)/i2c.c $(DEPS) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/i2c.c -o $(BUILDDIR)/i2c.o

$(BUILDDIR)/lcd.o: $(SRCDIR)/lcd.c $(DEPS) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/lcd.c -o $(BUILDDIR)/lcd.o

$(BUILDDIR)/main.o: $(SRCDIR)/main.c $(DEPS) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/main.c -o $(BUILDDIR)/main.o

$(BUILDDIR)/rtc.o: $(SRCDIR)/rtc.c $(DEPS) | $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/rtc.c -o $(BUILDDIR)/rtc.o

$(BUILDDIR)/ui/alarm.o: $(SRCDIR)/ui/alarm.c $(DEPS) | $(BUILDDIR)/ui
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/ui/alarm.c -o $(BUILDDIR)/ui/alarm.o

$(BUILDDIR)/ui/clock.o: $(SRCDIR)/ui/clock.c $(DEPS) | $(BUILDDIR)/ui
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/ui/clock.c -o $(BUILDDIR)/ui/clock.o

$(BUILDDIR)/ui/menu.o: $(SRCDIR)/ui/menu.c $(DEPS) | $(BUILDDIR)/ui
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/ui/menu.c -o $(BUILDDIR)/ui/menu.o

$(BUILDDIR)/ui/ui.o: $(SRCDIR)/ui/ui.c $(DEPS) | $(BUILDDIR)/ui
	$(CC) $(CFLAGS) $(INC) -c $(SRCDIR)/ui/ui.c -o $(BUILDDIR)/ui/ui.o

#Build directories

$(BUILDDIR)/ui: $(BUILDDIR)
	mkdir -p $(BUILDDIR)/ui

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

#Special phonies

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)

.PHONY: showSize
showSize: $(BUILDDIR)/a.out
	avr-size -C --mcu=atmega328p $(BUILDDIR)/a.out
	
.PHONY: flash
flash: $(BUILDDIR)/a.out.hex#Defaults: Alarm disabled, Timeout 5 seconds
	avrdude -c stk500v1 -P /dev/ttyUSB0 -p atmega328p -U flash:w:$(BUILDDIR)/a.out.hex:i -U eeprom:w:0x00,0x05:m -b19200 -v 
