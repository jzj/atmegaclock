#ifndef AVREEPROM_H
#define AVREEPROM_H

#include <stdint.h>

uint8_t AVREEPROM_read(uint16_t address);
#define AVREEPROM_write(data, address) do {AVREEPROM_swap(data, address);} while (0)
uint8_t AVREEPROM_swap(uint8_t data, uint16_t address);//Returns old data at address

#endif//AVREEPROM_H
