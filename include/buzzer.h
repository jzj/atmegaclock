#ifndef BUZZER_H
#define BUZZER_H

#include <avr/io.h>
#include <stdint.h>

void buzzer_init();
void buzzer_setFrequency(uint_fast16_t frequency);
void buzzer_enable();//TODO decide to just inline this/make into macro?
void buzzer_disable();//TODO decide to just inline this/make into macro?

#endif//BUZZER_H
